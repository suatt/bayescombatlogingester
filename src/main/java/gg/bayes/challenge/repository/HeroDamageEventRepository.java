package gg.bayes.challenge.repository;

import gg.bayes.challenge.rest.domain.HeroDamageEvent;
import gg.bayes.challenge.rest.model.HeroDamage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HeroDamageEventRepository extends CrudRepository<HeroDamageEvent, String> {
    @Query("SELECT distinct new gg.bayes.challenge.rest.model.HeroDamage(u.target, count(u.damage), sum(u.damage) ) FROM HeroDamageEvent u where   u.heroName=:heroName AND u.matchId=:matchId GROUP BY u.target")
    List<HeroDamage> findDamagesByHeroNameAndTarget(@Param("matchId") Long matchId, @Param("heroName") String heroName);
}
package gg.bayes.challenge.rest.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class CastSpellEvent {
    @Id
    @GeneratedValue
    private Long castSpellId;

    private Long matchId;
    private String heroName;
    private String spell;
    private Integer castLevel;

}

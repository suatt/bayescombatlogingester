package gg.bayes.challenge.service.impl;


import gg.bayes.challenge.errors.EntityNotFoundException;
import gg.bayes.challenge.repository.HeroDamageEventRepository;
import gg.bayes.challenge.repository.HeroItemEventRepository;
import gg.bayes.challenge.repository.HeroKillsEventRepository;
import gg.bayes.challenge.repository.HeroSpellsEventRepository;
import gg.bayes.challenge.rest.domain.CastSpellEvent;
import gg.bayes.challenge.rest.domain.HeroDamageEvent;
import gg.bayes.challenge.rest.domain.HeroItemsEvent;
import gg.bayes.challenge.rest.domain.HeroKillsEvent;
import gg.bayes.challenge.rest.model.HeroDamage;
import gg.bayes.challenge.rest.model.HeroItems;
import gg.bayes.challenge.rest.model.HeroKills;
import gg.bayes.challenge.rest.model.HeroSpells;
import gg.bayes.challenge.service.MatchService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@Slf4j
@Service
public class MatchServiceImpl implements MatchService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MatchServiceImpl.class);
    private static final String NPC_DOTA_HERO_ = "npc_dota_hero_";
    private static final String NPC_DOTA_ = "npc_dota_";
    private static final String KILLED_BY = "killed by";
    private static final String NPC_DOTA_HERO_Regex = "npc_dota_hero_\\s*(\\w+)";
    private static final Pattern heroNamePattern = Pattern.compile(NPC_DOTA_HERO_Regex);

    private HeroDamageEventRepository heroDamageEventRepository;
    private HeroItemEventRepository heroItemEventRepository;
    private HeroSpellsEventRepository heroSpellsEventRepository;
    private HeroKillsEventRepository heroKillsEventRepository;

    @Autowired
    public MatchServiceImpl(HeroDamageEventRepository heroDamageEventRepository, HeroItemEventRepository heroItemEventRepository,
                            HeroSpellsEventRepository heroSpellsEventRepository, HeroKillsEventRepository heroKillsEventRepository) {
        this.heroDamageEventRepository = heroDamageEventRepository;
        this.heroItemEventRepository = heroItemEventRepository;
        this.heroSpellsEventRepository = heroSpellsEventRepository;
        this.heroKillsEventRepository = heroKillsEventRepository;
    }

    @Override
    public List<HeroSpells> getSpells(Long matchId, String heroName) {
        LOGGER.info("getSpells service started");
        try {
            return heroSpellsEventRepository.findNumberOfCastsByHeroName(matchId, heroName);

        } catch (Exception ex) {
            throw new EntityNotFoundException(MatchServiceImpl.class, "getSpells not found for  " + heroName);
        }
    }

    @Override
    public List<HeroItems> getHeroItems(Long matchId, String heroName) {
        LOGGER.info("getHeroItems service started");
        try {

            return heroItemEventRepository.findHeroItemsDTOByHeroName1(matchId, heroName);
        } catch (Exception ex) {
            throw new EntityNotFoundException(MatchServiceImpl.class, "getHeroItems not found for  " + heroName);
        }
    }

    @Override
    public List<HeroDamage> getDamages(Long matchId, String heroName) {
        LOGGER.info("getDamages service started");
        return heroDamageEventRepository.findDamagesByHeroNameAndTarget(matchId, heroName);
    }

    @Override
    public List<HeroKills> getKills(Long matchID) {
        LOGGER.info("getKills service started");
        return heroKillsEventRepository.findKillsByHero(matchID);
    }


    @Override
    public Long ingestMatch(MultipartFile payload) {
        LOGGER.info("ingestMatch service started");
        if (payload.isEmpty()) {
            LOGGER.warn("File does not have any data");
            return Long.valueOf("file is empty");

        } else {

            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(payload.getInputStream()));
                Stream<String> lines = br.lines();
                String originalFilename = payload.getOriginalFilename();

                Pattern matchNumberPattern = Pattern.compile("\\d+");
                Matcher matchNumberMatcher = matchNumberPattern.matcher(originalFilename);

                long matchNumber = 0;
                if (matchNumberMatcher.find()) {
                    matchNumber = Long.parseLong(matchNumberMatcher.group());
                }

                long finalMatchNumber = matchNumber;
                lines.forEach(line ->
                        {
                            if (line.contains("buys")) {
                                purchaseExtractor(line, finalMatchNumber);
                            }
                            if (line.contains("casts")) {
                                castExtractor(line, finalMatchNumber);
                            }
                            if (line.contains("killed")) {
                                killExtractor(line, finalMatchNumber);
                            }
                            if (line.contains("hits")) {
                                damageExtractor(line, finalMatchNumber);
                            }
                        }
                );

            } catch (IOException io) {
                LOGGER.error("Error occurred wrong when uploadParameterCSVFile");
                io.printStackTrace();
            }
        }

        return 1L;
    }

    private void purchaseExtractor(String currentGameEvent, Long finalMatchNumber) {

        String itemName = "";
        HeroItemsEvent heroItemsEvent = new HeroItemsEvent();
        HeroItems heroItems = new HeroItems();
        String heroName = "";


        Pattern itemPattern = Pattern.compile("item_\\s*(\\w+)");

        Matcher itemMatcher = itemPattern.matcher(currentGameEvent);
        Matcher heroNameMatcher = heroNamePattern.matcher(currentGameEvent);

        if (itemMatcher.find()) {
            itemName = itemMatcher.group(1).trim();
        }

        if (heroNameMatcher.find()) {
            heroName = heroNameMatcher.group(1).trim();

        }

        String timeStamp = currentGameEvent.substring(currentGameEvent.indexOf('[') + 1, currentGameEvent.indexOf(']'));
        heroItems.setItem(itemName);
        heroItemsEvent.setHeroName(heroName);
        heroItemsEvent.setItem(itemName);
        heroItemsEvent.setMatchId(finalMatchNumber);

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parsedDate;

        long longFormattedDate = 1L;

        try {
            parsedDate = dateFormat.parse(timeStamp);
            longFormattedDate = parsedDate.getTime();
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        heroItemsEvent.setTimestamp(longFormattedDate);
        heroItemEventRepository.save(heroItemsEvent);
    }

    private void castExtractor(String currentGameEvent, Long matchNumber) {

        CastSpellEvent castSpellEvent = new CastSpellEvent();

        String castLevel = "";
        String castName = "";
        String heroName = "";

        Pattern castPattern = Pattern.compile("ability(.*?)\\(lvl");
        Matcher castLevelMatcher = getMatcher(currentGameEvent, "lvl(.*?)\\)");

        Matcher castNameMatcher = castPattern.matcher(currentGameEvent);
        Matcher heroNameMatcher = heroNamePattern.matcher(currentGameEvent);

        if (castLevelMatcher.find()) {
            castLevel = castLevelMatcher.group(1).trim();
        }

        if (castNameMatcher.find()) {
            castName = castNameMatcher.group(1).trim();
        }

        if (heroNameMatcher.find()) {
            heroName = heroNameMatcher.group(1).trim();

        }

        castSpellEvent.setCastLevel(Integer.valueOf(castLevel));
        castSpellEvent.setSpell(castName);
        castSpellEvent.setHeroName(heroName);
        castSpellEvent.setMatchId(matchNumber);
        heroSpellsEventRepository.save(castSpellEvent);
    }

    private void damageExtractor(String currentGameEvent, Long finalMatchNumber) {

        ArrayList<String> fighterList = new ArrayList<>();

        int damageAmount = 0;

        Pattern damageAmountPattern = Pattern.compile("(\\bfor\\b)(.*?)(\\bdamage\\b)");

        Pattern targetPattern = Pattern.compile("hits " + NPC_DOTA_HERO_Regex);

        Matcher damageAmountMatcher = damageAmountPattern.matcher(currentGameEvent);
        Matcher damagerNameMatcher = heroNamePattern.matcher(currentGameEvent);

        Matcher targetMatcher = targetPattern.matcher(currentGameEvent);

        if (damageAmountMatcher.find()) {
            String matcherResultString1 = damageAmountMatcher.group(0).trim();

            String damageAmountPattern1 = "(?<=for\\b)(.*?)(?=damage\\b)";

            Matcher damageAmountMatcher1 = getMatcher(matcherResultString1, damageAmountPattern1);

            String damageNumber;

            if (damageAmountMatcher1.find()) {
                damageNumber = damageAmountMatcher.group(2);
                damageAmount = Integer.parseInt(damageNumber.trim());
            }
        }

        if (damagerNameMatcher.find()) {
            String damager = damagerNameMatcher.group(1).replace(NPC_DOTA_HERO_, "");
            fighterList.add(damager);

        }

        if (targetMatcher.find()) {
            String target = targetMatcher.group(1).replace("hits " + NPC_DOTA_HERO_, "");
            fighterList.add(target);

        }

        String damager = fighterList.get(0);
        String target = fighterList.get(1);

        HeroDamageEvent heroDamageEvent = new HeroDamageEvent();
        heroDamageEvent.setHeroName(damager);
        heroDamageEvent.setTarget(target);
        heroDamageEvent.setDamage(damageAmount);
        heroDamageEvent.setMatchId(finalMatchNumber);

        heroDamageEventRepository.save(heroDamageEvent);
    }

    private Matcher getMatcher(String rawString, String regex1) {
        Pattern pattern1 = Pattern.compile(regex1);
        return pattern1.matcher(rawString);
    }

    private void killExtractor(String currentGameEvent, Long finalMatchNumber) {
        String killerHeroNameRegex = KILLED_BY + " " + NPC_DOTA_ + "\\s*(\\w+)";

        String rawString;

        String killer = "---------------------";
        String victim = "---------------------";

        Pattern killerHeroNamePattern = Pattern.compile(killerHeroNameRegex);

        Matcher killerHeroNameMatcher = killerHeroNamePattern.matcher(currentGameEvent);

        if (killerHeroNameMatcher.find()) {
            rawString = killerHeroNameMatcher.group();

            if (rawString.contains(NPC_DOTA_)) {
                killer = rawString.replaceAll(KILLED_BY + " " + NPC_DOTA_HERO_ + "|" + KILLED_BY + " " + NPC_DOTA_, "");

                String regex1 = "(npc_dota_hero_)(.*?)(is)|(npc_dota_)(.*?)(is)";
                Matcher matcher1 = getMatcher(currentGameEvent, regex1);

                String victimRawString;
                if (matcher1.find()) {
                    victimRawString = matcher1.group();
                    victim = victimRawString.replaceAll(NPC_DOTA_HERO_ + "|is|" + NPC_DOTA_ + "neutral_" + "|" + NPC_DOTA_, "").trim();
                }
            }
        }

        HeroKillsEvent heroKillsEvent = new HeroKillsEvent();
        heroKillsEvent.setKiller(killer);
        heroKillsEvent.setVictim(victim);
        heroKillsEvent.setMatchId(finalMatchNumber);
        heroKillsEventRepository.save(heroKillsEvent);

    }
}


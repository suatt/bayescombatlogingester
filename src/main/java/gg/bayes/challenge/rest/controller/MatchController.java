package gg.bayes.challenge.rest.controller;

import gg.bayes.challenge.rest.model.HeroDamage;
import gg.bayes.challenge.rest.model.HeroItems;
import gg.bayes.challenge.rest.model.HeroKills;
import gg.bayes.challenge.rest.model.HeroSpells;
import gg.bayes.challenge.service.MatchService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/match")
public class MatchController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MatchController.class);
    private final MatchService matchService;

    @Autowired
    public MatchController(MatchService matchService) {
        this.matchService = matchService;
    }

//    @PostMapping(consumes = "text/plain")
//    public ResponseEntity<Long> ingestMatch(@RequestBody @NotNull @NotBlank String payload) {
//        LOGGER.info("upload MatchController works ");
//        final Long matchId = matchService.ingestMatch(payload);
//        return ResponseEntity.ok(matchId);
//    }

    @PostMapping()
    public ResponseEntity<Long> ingestMatch(@NotNull @NotBlank @RequestParam("file") MultipartFile payload) {
        LOGGER.info("upload MatchController works ");
        final Long matchId = matchService.ingestMatch(payload);
        return ResponseEntity.ok(matchId);
    }

    @GetMapping("{matchId}")
    public ResponseEntity<List<HeroKills>> getMatch(@PathVariable("matchId") Long matchId) {
        LOGGER.info("getMatch MatchController starts ");
        List<HeroKills> kills = matchService.getKills(matchId);
        return new ResponseEntity<>(kills, HttpStatus.OK);
    }

    @GetMapping("{matchId}/{heroName}/items")
    public ResponseEntity<List<HeroItems>> getItems(@PathVariable("matchId") Long matchId,
                                                    @PathVariable("heroName") String heroName) {
        LOGGER.info("getItems MatchController starts ");
        List<HeroItems> heroItems = matchService.getHeroItems(matchId, heroName);
        return new ResponseEntity<>(heroItems, HttpStatus.OK);
    }

    @GetMapping("{matchId}/{heroName}/spells")
    public ResponseEntity<List<HeroSpells>> getSpells(@PathVariable("matchId") Long matchId,
                                                      @PathVariable("heroName") String heroName) {
        LOGGER.info("getSpells MatchController starts ");
        List<HeroSpells> spells = matchService.getSpells(matchId, heroName);
        return new ResponseEntity<>(spells, HttpStatus.OK);
    }

    @GetMapping("{matchId}/{heroName}/damage")
    public ResponseEntity<List<HeroDamage>> getDamage(@PathVariable("matchId") Long matchId,
                                                      @PathVariable("heroName") String heroName) {
        LOGGER.info("getDamage MatchController starts ");
        List<HeroDamage> damages = matchService.getDamages(matchId, heroName);
        return new ResponseEntity<>(damages, HttpStatus.OK);
    }
}

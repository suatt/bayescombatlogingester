package gg.bayes.challenge.repository;

import gg.bayes.challenge.rest.domain.HeroItemsEvent;
import gg.bayes.challenge.rest.model.HeroItems;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HeroItemEventRepository extends CrudRepository<HeroItemsEvent, String> {
    @Query("SELECT new gg.bayes.challenge.rest.model.HeroItems(u.item, u.timestamp) FROM HeroItemsEvent u where u.heroName=:heroName AND u.matchId=:matchId")
    List<HeroItems> findHeroItemsDTOByHeroName1(@Param("matchId") Long matchId, @Param("heroName") String heroName);
}


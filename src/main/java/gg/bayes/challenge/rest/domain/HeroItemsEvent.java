package gg.bayes.challenge.rest.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class HeroItemsEvent {

    @Id
    @GeneratedValue
    private long purchaseId;

    private String heroName;
    private Long matchId;
    private String item;
    private Long timestamp;
}

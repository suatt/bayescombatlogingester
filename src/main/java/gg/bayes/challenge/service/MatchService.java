package gg.bayes.challenge.service;

import gg.bayes.challenge.rest.model.HeroDamage;
import gg.bayes.challenge.rest.model.HeroItems;
import gg.bayes.challenge.rest.model.HeroKills;
import gg.bayes.challenge.rest.model.HeroSpells;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface MatchService {

    Long ingestMatch(MultipartFile payload);

    List<HeroSpells> getSpells(Long matchId, String heroName);

    List<HeroItems> getHeroItems(Long matchId, String heroName);

    List<HeroDamage> getDamages(Long matchId, String heroName);

    List<HeroKills> getKills(Long matchId);

}

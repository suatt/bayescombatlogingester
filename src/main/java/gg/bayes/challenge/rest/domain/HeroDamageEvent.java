package gg.bayes.challenge.rest.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class HeroDamageEvent {

    @Id
    @GeneratedValue
    long damageNumber;

    private String heroName;
    private Long matchId;
    private String target;
    private Integer damage;


}

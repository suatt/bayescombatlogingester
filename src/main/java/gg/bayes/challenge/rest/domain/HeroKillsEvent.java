package gg.bayes.challenge.rest.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class HeroKillsEvent {

    @Id
    @GeneratedValue
    private long murderId;

    private String killer;
    private Long matchId;
    private String victim;

}

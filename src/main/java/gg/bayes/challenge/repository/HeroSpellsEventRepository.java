package gg.bayes.challenge.repository;

import gg.bayes.challenge.rest.domain.CastSpellEvent;
import gg.bayes.challenge.rest.model.HeroSpells;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HeroSpellsEventRepository extends CrudRepository<CastSpellEvent, String> {
    @Query("SELECT DISTINCT new gg.bayes.challenge.rest.model.HeroSpells(u.spell, sum(u.castLevel)) FROM CastSpellEvent u WHERE u.heroName=:heroName AND u.matchId=:matchId GROUP BY u.spell")
    List<HeroSpells> findNumberOfCastsByHeroName(@Param("matchId") Long matchId, @Param("heroName") String heroName);
}




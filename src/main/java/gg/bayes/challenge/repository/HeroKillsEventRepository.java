package gg.bayes.challenge.repository;

import gg.bayes.challenge.rest.domain.HeroKillsEvent;
import gg.bayes.challenge.rest.model.HeroKills;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HeroKillsEventRepository extends CrudRepository<HeroKillsEvent, String> {
    @Query("SELECT  distinct new gg.bayes.challenge.rest.model.HeroKills(u.killer,  count(u.killer) )  FROM HeroKillsEvent u WHERE u.matchId=:matchId GROUP BY u.killer")
    List<HeroKills> findKillsByHero(@Param("matchId") Long matchId);
}